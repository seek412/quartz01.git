package com.demo.schedule;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author Administrator
 * @date 2017-11-23 下午 15:29
 */
public class TestJob {
    public static void main(String[] args) throws SchedulerException {
        //获取工厂
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        //调度器
        Scheduler scheduler = schedulerFactory.getScheduler();

        //定时任务A
        JobKey jobKeyA = new JobKey("jobA","groupA");
        JobDetail jobDetailA = JobBuilder.newJob(JobA.class)
                .withIdentity(jobKeyA)
                .build();
        //定时任务B
        JobKey jobKeyB = new JobKey("jobB","groupB");
        JobDetail jobDetailB = JobBuilder.newJob(JobB.class)
                .withIdentity(jobKeyB)
                .build();

        //定义触发的条件
        Trigger triggerA = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerNameA","groupA")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                .build();
        Trigger triggerB = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerNameB","groupB")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/1 * * * * ?"))
                .build();

        //4. 启动定时任务
        scheduler.start();
        scheduler.scheduleJob(jobDetailA,triggerA);
        scheduler.scheduleJob(jobDetailB,triggerB);

    }
}
