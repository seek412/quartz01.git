package com.demo.schedule;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 执行定时任务B
 * @author Administrator
 * @date 2017-11-23 下午 15:28
 */
public class JobB implements Job{

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("执行定时任务B...");
    }

}
